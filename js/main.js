//
//
// SWAP OUT CONTENT SECTIONS FOR SPA
//
//

var contentArea = $('#the-page'),
	onLoadURL = window.location.pathname,
	navEl = $('.ws-sitenav'),
	navElPress = navEl.find('.-ws-sitenav-press'),
	navElBrand = navEl.find('.-ws-sitenav-brand');

// make sure correct information is presented onload
if (onLoadURL == '/press-release') {
	contentArea.load('/partials/press.html');
	navElBrand.removeClass('accent');
	navElPress.addClass('accent');
} else if (onLoadURL == '/brand-assets') {
	contentArea.load('/partials/brand-assets.html');
	navElPress.removeClass('accent');
	navElBrand.addClass('accent');
}