# To get the site up & running

Pull down this repo, then start a local server in your local directory, for example `$ php -S localhost:1234`.

In a separate directory, pull down Asset Bazaar. You'll need to compile the LESS (instructions on how to do that are in the README.md on that repo). Then in your Asset Bazaar local copy, start up another server, for example: `$php -S localhost:9876`

Voila!